import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    frutas: [
      { id: 1, nombre: 'Manzana', cantidad: 0 },
      { id: 2, nombre: 'Pera', cantidad: 5 },
      { id: 3, nombre: 'Naranja', cantidad: 3 }
    ]
  },
  mutations: {
    aumentar(state, index) {
      state.frutas[index].cantidad++;
    },
    reiniciar(state) {
      state.frutas.forEach(elemento => {
        elemento.cantidad = 0;
      });
    }
  },
  actions: {
  },
  modules: {
  }
})
